import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'bloc/cubit/image_upload_field_cubit.dart';
import 'bloc/states/image_upload_field_state.dart';
import 'components/close_circular_button_widget.dart';
import 'core/app_color.dart';
import 'core/app_images.dart';
import 'models/image_upload_field_model.dart';

class ImageUploadField extends StatefulWidget {
  final String title;
  final String desc;
  final String buttonTitle;
  final EdgeInsets margin;
  final List<ImageUploadFieldModel> networkImages;
  final bool uploadMultipleImages;
  final bool isEditing;
  final bool isReadonly;
  final double height;
  final double width;

  ImageUploadField(
      {Key key,
      this.title,
      this.desc,
      this.buttonTitle,
      this.margin,
      this.networkImages,
      this.uploadMultipleImages = false,
      this.isEditing = true,
      this.isReadonly,
      this.height,
      this.width})
      : super(key: key) {
    assert(uploadMultipleImages != null);
  }

  @override
  ImageUploadFieldWidgetState createState() => ImageUploadFieldWidgetState();
}

class ImageUploadFieldWidgetState extends State<ImageUploadField> {
  List<ImageUploadFieldModel> newImages;
  List<ImageUploadFieldModel> deletedImages = List<ImageUploadFieldModel>();

  bool get isEditing =>
      !(widget.isReadonly ?? false) && (widget.isEditing ?? false);
  bool get isReadOnly => widget.isReadonly ?? false;

  final PageController _pageViewController = PageController();

  int currentImageIndex;

  @override
  Widget build(BuildContext context) => Container(
        padding: widget.margin ?? EdgeInsets.zero,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildLabel(),
            Container(
              width: widget.width ?? double.infinity,
              height: widget.height,
              margin: EdgeInsets.only(top: 5),
              decoration: BoxDecoration(
                  color: AppColor.FFDCEFFF_blue,
                  borderRadius: BorderRadius.circular(5)),
              child: BlocProvider(
                create: (context) => ImageUploadFieldCubit(
                    uploadMultipleImages: widget.uploadMultipleImages,
                    state: ImageUploadFieldState(
                        images: widget.networkImages
                            ?.map((e) => e..image = _createNetworkImages(e.url))
                            ?.toList(),
                        placeholder: widget.desc,
                        isEditing: isEditing)),
                child: Stack(
                  children: [
                    SizedBox(
                      height: 70,
                    ),
                    _createPlaceholder(),
                    _createImages(),
                    Positioned(
                        top: 0,
                        bottom: 0,
                        left: 0,
                        right: 0,
                        child: Align(
                            alignment: Alignment.center,
                            child: _createButton()))
                  ],
                ),
              ),
            )
          ],
        ),
      );

  Widget _createPlaceholder() =>
      BlocBuilder<ImageUploadFieldCubit, ImageUploadFieldState>(
          builder: (context, state) => state.showPlaceholder
              ? Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 33)
                      .copyWith(top: 16, bottom: 76),
                  child: Text(
                    widget.desc ?? '',
                    style: TextStyle(
                        color: AppColor.FF4682B4_PrimaryColor, fontSize: 16),
                    textAlign: TextAlign.center,
                  ),
                )
              : SizedBox.shrink());

  // leff arrow
  Widget _createLeftArrow({BuildContext context, bool visible}) => Visibility(
        visible: visible,
        child: _createArrow(
            iconData: Icons.chevron_left,
            onTap: () {
              _pageViewController.previousPage(
                  duration: Duration(milliseconds: 300), curve: Curves.easeIn);
              context.read<ImageUploadFieldCubit>().setCurrentImageIndex(-1);
            }),
      );

  // right arrow
  Widget _createRightArrow({BuildContext context, bool visible}) => Visibility(
        visible: visible,
        child: _createArrow(
            iconData: Icons.chevron_right,
            onTap: () {
              _pageViewController.nextPage(
                  duration: Duration(milliseconds: 300), curve: Curves.easeIn);
              context.read<ImageUploadFieldCubit>().setCurrentImageIndex(1);
            }),
      );

  Widget _createImages() =>
      BlocBuilder<ImageUploadFieldCubit, ImageUploadFieldState>(
          builder: (context, state) {
        newImages = state.images;

        currentImageIndex = (state.images ?? []).isEmpty ? null : 0;

        return state.showImages
            ? Stack(
                children: [
                  PageView(
                      controller: _pageViewController,
                      physics: NeverScrollableScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      onPageChanged: (index) => currentImageIndex = index,
                      children: [
                        ...(state.images)
                            .map<Widget>((e) => Center(child: e.image))
                            .toList(),
                      ]),
                ],
              )
            : SizedBox.shrink();
      });

  Widget _createNetworkImages(String url) => FadeInImage(
        image: NetworkImage(url ?? ''),
        placeholder: AssetImage(
            AppImages.imagePlaceholder), // TODO it is currently empty
        fit: BoxFit.contain,
        placeholderErrorBuilder: (context, error, stackTrace) =>
            Opacity(opacity: 0.1, child: Placeholder()),
      );

  Widget _createButton() =>
      BlocBuilder<ImageUploadFieldCubit, ImageUploadFieldState>(
        builder: (context, state) {
          final _cubit = context.watch<ImageUploadFieldCubit>();
          return Center(
            child: FittedBox(
              fit: BoxFit.contain,
              child: Row(
                children: [
                  _createLeftArrow(
                    context: context,
                    visible: state.showLeftArrow,
                  ),
                  _createSetImageButton(
                      isEditing: state.isEditing,
                      onTap: state.isEditing
                          ? () => _cubit.addImage(context)
                          : _cubit.switchToEditMode),
                  FittedBox(
                      fit: BoxFit.contain,
                      child: _buildCloseButton(
                          context: context,
                          imageModel: currentImageIndex == null
                              ? null
                              : state.images[currentImageIndex])),
                  _createRightArrow(
                    context: context,
                    visible: state.showRightArrow,
                  ),
                ],
              ),
            ),
          );
        },
      );

  Widget _createSetImageButton({bool isEditing, Function onTap}) => Visibility(
        visible: !isReadOnly,
        child: InkWell(
          onTap: onTap,
          child: FittedBox(
            fit: BoxFit.contain,
            child: Padding(
              padding: const EdgeInsets.all(5),
              child: Container(
                padding: const EdgeInsets.all(5),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                ),
                child: !isEditing
                    ? Icon(
                        Icons.edit,
                        color: AppColor.FF4682B4_PrimaryColor,
                      )
                    : Icon(Icons.upload_file,
                        color: AppColor.FF4682B4_PrimaryColor),
              ),
            ),
          ),
        ),
      );

  void _deleteImage(ImageUploadFieldModel image, BuildContext context) {
    deletedImages.add(image);
    context.read<ImageUploadFieldCubit>().deleteImage(image.id);
  }

  Widget _buildCloseButton(
          {BuildContext context, ImageUploadFieldModel imageModel}) =>
      isEditing && imageModel != null
          ? Padding(
              padding: const EdgeInsets.all(5),
              child: CloseCircularButtonWidget(
                onClose: () => _deleteImage(imageModel, context),
              ),
            )
          : SizedBox(
              width: 1,
              height: 1,
            );

  Widget _createArrow({IconData iconData, Function onTap}) => InkWell(
        onTap: onTap,
        child: FittedBox(
          fit: BoxFit.contain,
          child: Container(
              alignment: Alignment.center,
              decoration:
                  BoxDecoration(color: Colors.black, shape: BoxShape.circle),
              child: Icon(
                iconData,
                color: Colors.white,
              )),
        ),
      );

  _buildLabel() => widget.title is String
      ? Text(
          widget.title,
          style: TextStyle(
              color: AppColor.FF4682B4_PrimaryColor,
              fontSize: 16,
              fontWeight: FontWeight.w500),
        )
      : SizedBox.shrink();
}
