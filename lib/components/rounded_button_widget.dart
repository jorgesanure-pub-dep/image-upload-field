import 'package:flutter/material.dart';
import 'package:image_upload_field/core/app_color.dart';

class RoundedButtonWidget extends StatelessWidget {
  final String title;
  final Widget child;
  final EdgeInsets margin;
  final Color bgColor;
  final Color textColor;
  final Function onTap;
  final Widget leading;
  final TextStyle textStyle;

  const RoundedButtonWidget(
      {Key key,
      this.title,
      this.margin,
      this.bgColor,
      this.textColor,
      this.onTap,
      this.leading,
      this.textStyle,
      this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget _leading;
    if (leading != null)
      _leading = Padding(
        padding: EdgeInsets.only(right: 5),
        child: leading,
      );

    final _textStyle = textStyle ?? TextStyle();

    return InkWell(
      onTap: onTap,
      child: Container(
        margin: margin,
        padding: EdgeInsets.symmetric(vertical: 9, horizontal: 25),
        decoration: BoxDecoration(
            color: bgColor ?? AppColor.FF4682B4_PrimaryColor,
            borderRadius: BorderRadius.horizontal(
              left: Radius.circular(23),
              right: Radius.circular(23),
            ),
            boxShadow: [
              BoxShadow(
                  offset: Offset(0, 2.5),
                  blurRadius: 5,
                  color: AppColor.shadow_gray_70_130_180_28)
            ]),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            _leading ?? SizedBox.shrink(),
            child ??
                Text(
                  title,
                  style: _textStyle.copyWith(
                      color: textStyle?.color ?? textColor ?? Colors.white,
                      fontSize: textStyle?.fontSize ?? 18,
                      fontWeight: textStyle?.fontWeight ?? FontWeight.w600),
                ),
          ],
        ),
      ),
    );
  }
}
