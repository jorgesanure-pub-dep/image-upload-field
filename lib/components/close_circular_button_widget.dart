import 'package:flutter/material.dart';
import 'package:image_upload_field/core/app_color.dart';

class CloseCircularButtonWidget extends StatelessWidget {
  final Function onClose;

  const CloseCircularButtonWidget({Key key, this.onClose}) : super(key: key);

  @override
  Widget build(BuildContext context) => InkWell(
        onTap: onClose,
        child: Container(
          width: 28,
          height: 28,
          decoration: BoxDecoration(
              shape: BoxShape.circle, color: AppColor.FFFF0000_red),
          child: Icon(
            Icons.close,
            color: Colors.white,
          ),
        ),
      );
}
