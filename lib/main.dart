import 'dart:math';

import 'package:flutter/material.dart';

import 'image_upload_field.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) => MaterialApp(
      title: 'Image Upload Field Demo',
      home: SafeArea(
        child: Builder(
          builder: (context) {
            final _size = MediaQuery.of(context).size;
            return Scaffold(
              body: Center(
                child: SingleChildScrollView(
                  child: Wrap(
                    runSpacing: 10,
                    spacing: 5,
                    children: List.generate(
                        4,
                        (index) => ImageUploadField(
                              width: _getRandNumber(_size.width),
                              height: _getRandNumber(_size.height),
                              uploadMultipleImages: true || Random().nextBool(),
                            )),
                  ),
                ),
              ),
            );
          },
        ),
      ));

  _getRandNumber(double size, {double min}) {
    min ??= 0.4;
    double randomixer = Random().nextDouble();
    if (randomixer < min) randomixer = min;
    return (size / 2) * randomixer;
  }
}
