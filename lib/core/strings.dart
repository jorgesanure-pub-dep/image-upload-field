class Strings {
  Strings._();
  static const String imageGallery = 'Galería de imágenes';
  static const String camera = 'Cámara';
  static const String edit = 'Editar';
  static const String uploadImages = 'Subir imágenes';
  static const String uploadImage = 'Subir Imagen';
}
