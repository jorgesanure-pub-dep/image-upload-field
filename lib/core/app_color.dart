import 'package:flutter/material.dart';

class AppColor {
  AppColor._();
  static const Color FF4682B4_PrimaryColor = Color(0xFF4682B4);
  static const Color FFFF0000_red = Color(0xFFFF0000);
  static const Color shadow_gray_70_130_180_28 =
      Color.fromRGBO(70, 130, 180, 0.28);
  static const Color FFDCEFFF_blue = Color(0xFFDCEFFF);
}
