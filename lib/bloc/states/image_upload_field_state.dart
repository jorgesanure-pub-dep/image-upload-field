import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:image_upload_field/models/image_upload_field_model.dart';

part 'image_upload_field_state.g.dart';

@CopyWith()
class ImageUploadFieldState {
  final String placeholder;
  final bool isEditing;
  int currentImageIndex;

  bool get showPlaceholder => _isEmptyImages && _isNotEmptyPlaceholder;
  bool get showImages => !_isEmptyImages;
  bool get hasOnlyOneImage => (images?.length ?? 0) == 1;
  bool get _isEmptyImages => images?.isEmpty ?? true;
  bool get _isNotEmptyPlaceholder => (placeholder?.isNotEmpty ?? false);

  bool get showLeftArrow => !_isEmptyImages && currentImageIndex > 0;

  bool get showRightArrow =>
      !_isEmptyImages && currentImageIndex != (images.length - 1);

  List<ImageUploadFieldModel> images = <ImageUploadFieldModel>[];

  ImageUploadFieldState(
      {int currentImageIndex = 0,
      this.isEditing = false,
      this.placeholder,
      this.images}) {
    this.currentImageIndex = !_isEmptyImages &&
            currentImageIndex != null &&
            currentImageIndex >= 0 &&
            currentImageIndex <= images.length
        ? currentImageIndex
        : this.currentImageIndex;

    if (this.currentImageIndex == null && !_isEmptyImages)
      this.currentImageIndex = 0;
  }
}
