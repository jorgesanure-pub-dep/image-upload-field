// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'image_upload_field_state.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension ImageUploadFieldStateCopyWith on ImageUploadFieldState {
  ImageUploadFieldState copyWith({
    int currentImageIndex,
    List<ImageUploadFieldModel> images,
    bool isEditing,
    String placeholder,
  }) {
    return ImageUploadFieldState(
      currentImageIndex: currentImageIndex ?? this.currentImageIndex,
      images: images ?? this.images,
      isEditing: isEditing ?? this.isEditing,
      placeholder: placeholder ?? this.placeholder,
    );
  }
}
