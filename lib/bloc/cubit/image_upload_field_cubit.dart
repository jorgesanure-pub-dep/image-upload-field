import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_upload_field/bloc/states/image_upload_field_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_upload_field/core/strings.dart';
import 'package:image_upload_field/models/image_upload_field_model.dart';

class ImageUploadFieldCubit extends Cubit<ImageUploadFieldState> {
  final bool uploadMultipleImages;

  final picker = ImagePicker();

  ImageUploadFieldCubit(
      {this.uploadMultipleImages, ImageUploadFieldState state})
      : super(state);

  void addImage(BuildContext context) {
    _showPicker(context).then((value) {
      if (value?.image != null) {
        final List<ImageUploadFieldModel> _images =
            uploadMultipleImages ? [...(state.images ?? []), value] : [value];
        emit(state.copyWith(images: _images));
      }
    });
  }

  void switchToEditMode() => emit(state.copyWith(isEditing: true));

  void deleteImage(int id) => emit(
      state.copyWith(images: state.images..removeWhere((i) => i.id == id)));

  Future<ImageUploadFieldModel> _showPicker(context) => showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          child: Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                    leading: new Icon(Icons.photo_library),
                    title: new Text(Strings.imageGallery),
                    onTap: () async {
                      await _getImageFromGallery().then((value) {
                        return Navigator.of(context)
                            .pop<ImageUploadFieldModel>(value);
                      });
                    }),
                new ListTile(
                  leading: new Icon(Icons.photo_camera),
                  title: new Text(Strings.camera),
                  onTap: () {
                    _getImageFromCamera().then((value) => Navigator.of(context)
                        .pop<ImageUploadFieldModel>(value));
                  },
                ),
              ],
            ),
          ),
        );
      });

  Future<ImageUploadFieldModel> _getImageFromCamera() =>
      _getImage(ImageSource.camera);
  Future<ImageUploadFieldModel> _getImageFromGallery() =>
      _getImage(ImageSource.gallery);

  Future<ImageUploadFieldModel> _getImage(ImageSource source) =>
      picker.getImage(source: source, imageQuality: 50).then((pickedFile) {
        Image image;
        if (pickedFile != null) {
          image = Image.file(File(pickedFile.path));
        }
        return ImageUploadFieldModel(image: image, path: pickedFile?.path);
      });

  void setCurrentImageIndex(index) =>
      emit(state.copyWith(currentImageIndex: state.currentImageIndex + index));
}
