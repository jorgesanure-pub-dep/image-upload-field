import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:flutter/material.dart';

part 'image_upload_field_model.g.dart';

@CopyWith()
class ImageUploadFieldModel {
  static int _id = 0;
  final int id = ++_id;
  Widget image;
  String path; // filename from devise
  String url; // network url
  ImageUploadFieldModel({this.image, this.path, this.url});
}
