// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'image_upload_field_model.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension ImageUploadFieldModelCopyWith on ImageUploadFieldModel {
  ImageUploadFieldModel copyWith({
    Widget image,
    String path,
    String url,
  }) {
    return ImageUploadFieldModel(
      image: image ?? this.image,
      path: path ?? this.path,
      url: url ?? this.url,
    );
  }
}
